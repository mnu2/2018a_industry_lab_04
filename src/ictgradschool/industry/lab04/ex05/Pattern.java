package ictgradschool.industry.lab04.ex05;

import java.security.PrivateKey;

public class Pattern {
    private int num;
    private char symb;

    Pattern(int num, char symb){
        this.num = num;
        this.symb = symb;
    }
/*the toString() method just needs to make a string of length num consisting of the symb
* characters*/
    public String toString(){
        String string = new String("");
        for (int i = 0; i < num; i++){
            string += symb;
        }
        return string;
    }

    public void setNumberOfCharacters(int num){
        this.num = num;
    }

    public int getNumberOfCharacters(){
        return num;
    }
}
