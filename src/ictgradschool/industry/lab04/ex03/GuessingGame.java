package ictgradschool.industry.lab04.ex03;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {
    private int goal = (int)(100*Math.random());
    private int guess = 0;

    public void start() {
        getGuess();
        while (guess!= goal)
        {
            if (guess <= goal){
                System.out.println("Too low, try again");
            }
            else {
                System.out.println("Too high, try again");
            }
            getGuess();
        }
        System.out.println("Perfect!");
        System.out.println("Goodbye");

        // TODO Write your code here.
    }

    private int getGuess(){
        System.out.print("Enter your guess (1 - 100): ");
        this.guess = Integer.parseInt(Keyboard.readInput());
        return guess;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
