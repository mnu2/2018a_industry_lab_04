package ictgradschool.industry.lab04.ex04;

import com.sun.javafx.scene.control.skin.ChoiceBoxSkin;
import ictgradschool.Keyboard;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;
    public static final int QUIT = 4;
    private String playerName;
    private int playerChoice;
    private int computerChoice;

    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.
        System.out.print("Hi!  What is your name?");
        playerName = Keyboard.readInput();
        System.out.print("1. Rock\n2.Scissors\n3. Paper\n4. Quit\nEnter choice: ");
        playerChoice = Integer.parseInt(Keyboard.readInput());

        while (playerChoice != 4) {
            computerChoice = (int)(3*Math.random()) + 1;
            displayPlayerChoice(playerName, playerChoice);
            displayPlayerChoice("Computer", computerChoice);
            System.out.println(getResultString(playerChoice, computerChoice));
            System.out.print("1. Rock\n2.Scissors\n3. Paper\n4. Quit\nEnter choice: ");
            playerChoice = Integer.parseInt(Keyboard.readInput());
        }
        displayPlayerChoice(playerName, playerChoice);
    }


    public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)
/*        if (choice == ROCK) {
            System.out.println(name + " chose rock." );
        }
        else if (choice == SCISSORS){
            System.out.println(name + " chose scissors." );
        }
        else if (choice == PAPER){
            System.out.println(name + " chose paper." );
        }
        else {
            System.out.println("Goodbye " + name + ".  Thanks for playing :)");
        }*/
    switch(choice){
        case ROCK:
            System.out.println(name + " chose rock." );
            break;

        case SCISSORS:
            System.out.println(name + " chose scissors." );
            break;

        case PAPER:
            System.out.println(name + " chose paper." );
            break;

        case QUIT:
            System.out.println("Goodbye " + name + ".  Thanks for playing :)" );
            break;
    }


    }

    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.
        if (playerChoice == ROCK && computerChoice == SCISSORS){

            return true;
        }
        else if (playerChoice == SCISSORS && computerChoice == PAPER){

            return true;
        }
        else if (playerChoice == PAPER && computerChoice == ROCK){

            return true;
        }

        return false;
    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = "you chose the same as the computer";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        if (userWins(playerChoice, computerChoice)) {

            if (playerChoice == ROCK && computerChoice == SCISSORS) {
                return playerName + " wins because " + ROCK_WINS + "\n";
            } else if (playerChoice == SCISSORS && computerChoice == PAPER) {
                return playerName + " wins because " + SCISSORS_WINS + "\n";
            } else if (playerChoice == PAPER && computerChoice == ROCK) {
                return playerName + " wins because " + PAPER_WINS + "\n";
            }
        }
        else {
            if (computerChoice == ROCK && playerChoice == SCISSORS) {
                return "Computer wins because " + ROCK_WINS + "\n";
            } else if (computerChoice == SCISSORS && playerChoice == PAPER) {
                return "Computer wins because " + SCISSORS_WINS + "\n";
            } else if (computerChoice == PAPER && playerChoice == ROCK) {
                return "Computer wins because " + PAPER_WINS + "\n";
            }
            else {
                return "No one wins because " + TIE + "\n";
            }
        }
        return null;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
